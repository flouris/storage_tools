
/* Original work Copyright (c) 2016, Michail Flouris <michail@flouris.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Rambler BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 * DESCRIPTION: This tool compares data blocks across two block devices (or files),
 *              opening then with Direct IO enabled and reading them in chunks of 256KB.
 *
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include <getopt.h>
#include <stdbool.h>

// FIXME: buffered (non direct I/O) support is not complete!
#define USE_DIRECT_IO

#ifdef USE_DIRECT_IO
#define BLOCKSIZE 65536
#else
#define BLOCKSIZE 65000
#error Not implemented yet!
#endif

#define MAXDEVS	8 // up to 8 devices currently

#undef ENABLE_DEBUG

/* ________________________________________________________________________________ */

/* Read a whole block of data (no offset in the block) */
int
read_block( int devfd, char *data, unsigned long long blockno, int bsize )
{
	int rbytes, bytes_read = 0, unread_bytes, offset = 0;
	unsigned long long boff = blockno * (unsigned long long)bsize + offset;

	assert( devfd > 0 && bsize > 0 );
	assert( data != NULL );

	//fprintf( stderr,"Reading %llu bytes @ offset: %llu [Block No: %llu, Offset: %llu]\n", bsize, boff, blockno, offset);

	for ( unread_bytes = (int)bsize; unread_bytes > 0; unread_bytes -= rbytes ) {

		//fprintf( stderr,"unread_bytes: %d offset: %lld, rbytes: %d\n", unread_bytes, boff, rbytes);

        rbytes = pread( devfd, data+bytes_read, (size_t)unread_bytes, boff+(unsigned long long)bytes_read);

        if (rbytes < 0) {
			fprintf( stderr,"Error reading from offset %llu -> pread() error: %s\n", boff, strerror(errno) );
#ifdef ENABLE_DEBUG
			fprintf( stderr,"DEBUG: fd: %d, unread: %d, offset: %llu, read: %d, bsize: %llu\n",
							devfd, unread_bytes, boff, bytes_read, bsize);
#endif
			return 0;
        }
		else if (rbytes == 0) {
			fprintf( stderr,"Error reading (EOF) at offset %llu -> pread() error: %s\n", boff, strerror(errno) );
			return 2; /* EOF */
		}

        bytes_read += rbytes;
    }

    assert ( unread_bytes == 0 );
	return 1;
}


typedef enum {
	access_sequential = 1,
	access_reverse_seq = 2,
	access_random_chunks = 3,
}	read_order_t;

unsigned chunk_size_blocks = 60;
unsigned long long	*rearr_chunk_tbl = NULL, rearr_chunk_cnt;

const char *
read_order_to_string( read_order_t read_order)
{
	switch (read_order) {
	case access_random_chunks:
		return "random chunks";
	case access_sequential:
		return "sequential access";
	case access_reverse_seq:
		return "reverse sequential";
	default:
		return "ERROR: UNKNOWN!";
	}
}

bool
swap_chunks( unsigned long long	*rearr_chunk_tbl,
			 unsigned long long rearr_chunk_cnt,
			 unsigned first, unsigned second )
{
	unsigned long long tmp;

	if (first < 0 || second < 0 ||
		first >= rearr_chunk_cnt || second >= rearr_chunk_cnt)
		return false;
	
	assert( rearr_chunk_tbl[first] >= 0 && rearr_chunk_tbl[first] < rearr_chunk_cnt );
	assert( rearr_chunk_tbl[second] >= 0 && rearr_chunk_tbl[second] < rearr_chunk_cnt );
	tmp = rearr_chunk_tbl[first];
	rearr_chunk_tbl[first] = rearr_chunk_tbl[second];
	rearr_chunk_tbl[second] = tmp;
#ifdef ENABLE_DEBUG
	fprintf(stderr,"tbl[%d] = %lld <-> tbl[%d] = %lld\n", first, rearr_chunk_tbl[first],
		second, rearr_chunk_tbl[second] );
#endif
	return true;
}


/* ATTENTION: this function should cover all block addresses for all indexes between
 *            0 and maxblkaddr... easiest solution would be to
 */
unsigned long long
get_block_addr_from_idx( unsigned long long blkidx,
                         unsigned long long maxblkaddr, read_order_t read_order )
{
	int i, rearr_chunk_idx;

	switch (read_order) {
	case access_random_chunks:
		/* non-sequential access pattern... it's not completely random, but mostly alternating chunks
		   from the start and end of disk block range... */
	
	/* if chunk rearrangement table does not exist, create it... */
	if ( !rearr_chunk_tbl ) {

		/* try to adjust chunk_size_blocks to a zero mod of blocks... */
		while (maxblkaddr % chunk_size_blocks && chunk_size_blocks > 7) {
			chunk_size_blocks--;
		}

		if (maxblkaddr % chunk_size_blocks) {
			read_order = access_sequential;
			return blkidx;
		}

		rearr_chunk_cnt = maxblkaddr / chunk_size_blocks;
		if ( rearr_chunk_cnt < 4 ) { /* for very small sizes, just do sequential... */
			read_order = access_sequential;
			return blkidx;
		}

		rearr_chunk_tbl = (unsigned long long *) malloc( rearr_chunk_cnt * sizeof(*rearr_chunk_tbl) );
		if ( !rearr_chunk_tbl ) {
			fprintf( stderr, "Unable to alloc memory for chunk_tbl, error: %s\n", strerror(errno) );
			exit(EXIT_FAILURE);
		}
		memset( rearr_chunk_tbl, 0, rearr_chunk_cnt * sizeof(*rearr_chunk_tbl));

		for (i = 0; i < rearr_chunk_cnt; i++ )
			rearr_chunk_tbl[i] = i;

		/* NOTE: swap chunks around the center of the address space...
		 *
		 * ATTENTION: to get reproducible pattern, one must use THE SAME input block range (address space)!
		 */
		for (i = 0; i < rearr_chunk_cnt / 2; i+=2 )
			swap_chunks( rearr_chunk_tbl, rearr_chunk_cnt, i, rearr_chunk_cnt-i-1);
	}

	rearr_chunk_idx = blkidx / chunk_size_blocks;
	if (rearr_chunk_idx >= 0 && rearr_chunk_idx < rearr_chunk_cnt) {
		assert (rearr_chunk_tbl[rearr_chunk_idx] >= 0 &&
				rearr_chunk_tbl[rearr_chunk_idx] < rearr_chunk_cnt );
		return (rearr_chunk_tbl[rearr_chunk_idx] * chunk_size_blocks) + (blkidx % chunk_size_blocks);
	}

	break;
	case access_sequential:
		return blkidx;
	case access_reverse_seq:
		return (maxblkaddr - 1 - blkidx) >= 0 ? (maxblkaddr - 1 - blkidx) : blkidx;
	default:
		assert(read_order >= 1 && read_order <= 3);
	break;
	}

	return blkidx;
}

static struct timeval  gtv1, gtv2;
static struct timezone gtz;

/****************  START TIMER   **********************************/
int
start_timer( void )
{
	if (gettimeofday( &gtv1, &gtz) == -1) {
		fprintf( stderr, "Error in gettimeofday (start_timer)\n");
		return 0;
	}
	return 1;
}
/******************************************************************/

long long
stop_timer_get_usec( void )
{
	long long elapsed_usecs;

	if (gettimeofday( &gtv2, &gtz) == -1) {
		fprintf( stderr, "Error in gettimeofday (stop_timer)\n");
		return 0;
	}

	elapsed_usecs = ((long long)(gtv2.tv_sec - gtv1.tv_sec)) * 1000000LL +
					((long long)(gtv2.tv_usec - gtv1.tv_usec));
	assert( elapsed_usecs > 0 );

	return elapsed_usecs;
}

/******************************************************************/
void
print_time_results( char * testid, int reps, int devcount, long long *rusecs )
{
	long long elapsed_usecs;
	double  elapsed_time;

	elapsed_usecs = ((long long)(gtv2.tv_sec - gtv1.tv_sec)) * 1000000LL +
					((long long)(gtv2.tv_usec - gtv1.tv_usec));
	assert( elapsed_usecs > 0 );

	elapsed_time = (((double) (gtv2.tv_sec - gtv1.tv_sec) * 1000000.0) +
					((double) (gtv2.tv_usec - gtv1.tv_usec))) / (double) 1000000.0;

	*rusecs = elapsed_usecs;

	printf("\n\"%s\" => TOTAL TIME    : %.6f secs, %lld usecs.\n", testid, elapsed_time, elapsed_usecs );
	printf(  "\"%s\" => AVG TIME/CALL : %.6f secs, %lld usecs.\n", testid,
			elapsed_time / (double)reps, elapsed_usecs / reps );
	printf("====================================================================================\n");
}

/******************************************************************/
void print_usage( char *exename )
{
	printf("Replica Data Check v.0.1\n");
	printf("Usage: %s <options> <device 1 path> ... <device N path> (2 - 4 device paths)\n", exename);
	printf("Options: [-c | --cycles <cycle no (default: 1)>]\n");
	printf("         [-m | --mbrange <data size (MBytes, default: 1024)>]\n");
	printf("         [-s | --startaddr <start block addr (default: 0)>]\n");
	printf("         [-o | --readorder <order: 1=sequential(default),2=reverse_sequential,3:random_chunks>]\n");
	printf("         [-v | --verbose]\n");
	printf("Example: %s -s 1 -m 100 /dev/nbd1 /dev/nbd2\n", exename);
	printf("NOTE: This tool only READS data, does not write/modify any data on the devices!\n");
}

/* ________________________________________________________________________________ */

int
main(int argc, char **argv)
{
	int i, devfd[MAXDEVS], devcount = 0, verbose = 0, errcount = 0, cycles = 1, cycle, err;
	unsigned long long bn, mbrange = 1024, max_baddr = 0, startaddr = 0;
	read_order_t read_order = access_sequential;

	/* Deal with arguments: <device 1 path> ... <device N path> */
	char *devname[MAXDEVS], *readbufs[MAXDEVS];
	long long rd_usecs[MAXDEVS];

	/*________ start of option processing... _________ */
	int option_index = 0;

	int c;

	static struct option long_options[] = {
		{"cycles", 1, 0, 0},
		{"mbrange", 1, 0, 0},
		{"readorder", 1, 0, 0}, /* ATTENTION: default is sequential access! */
		{"verbose", 0, 0, 0},
		{0, 0, 0, 0}
	};

	if ( argc < 3 ) // min 3 args
		goto error_parsing_options;

	while ( (c = getopt_long (argc, argv, "c:m:s:o:v",
                        long_options, &option_index) ) != -1 ) {

		//int this_option_optind = optind ? optind : 1;
		int option_index = 0;

		switch (c) {
		case 0:
			//printf ("option \"%s\"", long_options[option_index].name);
			//if (optarg)
			//	printf (" with arg %s", optarg);
			//printf ("\n");

			if ( strncmp(long_options[option_index].name,"cycles",7) == 0 )
				goto cycles_option;
			else if ( strncmp(long_options[option_index].name,"verbose",8) == 0 )
				goto verbose_option;
			else if ( strncmp(long_options[option_index].name,"mbrange",8) == 0 )
				goto mbrange_option;
			else if ( strncmp(long_options[option_index].name,"startaddr",8) == 0 )
				goto startaddr_option;
			else if ( strncmp(long_options[option_index].name,"readorder",10) == 0 )
				goto readorder_option;
		break;

		case 'c':
cycles_option:
			//printf ("Setting cycles = %s\n", optarg);
			if ( sscanf( optarg, "%d", &i ) != 1 )
				goto error_parsing_options;
			cycles = i;
		break;

		case 'm':
mbrange_option:
			//printf ("Setting max block addr to check = %s\n", optarg);
			if ( sscanf( optarg, "%lld", &bn ) != 1 )
				goto error_parsing_options;
			mbrange = bn;
		break;

		case 's':
startaddr_option:
			//printf ("Setting start address to = %s\n", optarg);
			if ( sscanf( optarg, "%lld", &startaddr ) != 1 )
				goto error_parsing_options;
		break;

		case 'o':
readorder_option:
			//printf ("Setting read order to = %s\n", optarg);
			if ( sscanf( optarg, "%d", &i ) != 1 )
				goto error_parsing_options;
			if (i >= 1 && i <= 3)
				read_order = i; /* set read order (IO access pattern) - default is random */
		break;

		case 'v':
verbose_option:
			verbose = 1;
		break;

		default:
			printf ("Unknown option(s)... (character code 0%o ?). Aborting...\n", c);
error_parsing_options:
			print_usage(argv[0]);
			exit(-1);
		}
	} /* while... */

	devcount = 0;
	memset( devname, 0, MAXDEVS*sizeof(char) );
	memset( devfd, -1, MAXDEVS*sizeof(int) );

	/* ok, now get arguments not in the options... */
	if (optind < argc) {
		//printf ("non-option ARGV-elements: ");
		while (optind < argc) {
			char * noptarg = argv[optind++];
			//printf ("%s %d, ", noptarg, devcount);

			/* we need at least two arguments as devices to compare! */
			if (devcount < MAXDEVS ) {
				assert( strlen(noptarg) < 64 );
				devname[devcount] = malloc( strlen(noptarg)+1 );
				memset( devname[devcount], 0, strlen(noptarg)+1 );
				sprintf( devname[devcount], "%s", noptarg );
				devcount++;
			}
			else if ( devcount >= MAXDEVS ) {
				fprintf( stderr, "Error: Too many test devices (max: %d!\n", MAXDEVS);
				exit(-1);
			} else {
				printf ("Invalid argument: %s. Aborting...\n", noptarg);
				print_usage(argv[0]);
				exit(-1);
			}
		}
   	}
	/*________ end of option processing... _________ */

	max_baddr = (mbrange * (1024*1024)) / BLOCKSIZE;

	if (read_order == access_random_chunks && max_baddr % chunk_size_blocks ) {
		max_baddr -= max_baddr % chunk_size_blocks;
		mbrange = (max_baddr * BLOCKSIZE) / (1024*1024);
		fprintf( stderr, "NOTE: Adjusting max block to %lld (%lld MBytes)\n", max_baddr, mbrange );
	}

	printf("Settings: cycles = %d, start_addr = %lld, read_order = %d (%s), mbrange = %lld MB (max block %lld)\n",
				cycles, startaddr, read_order, read_order_to_string( read_order ), mbrange, max_baddr );

	if ( startaddr >= max_baddr || startaddr > (1 << 16) || max_baddr > (1 << 24) ) {
		fprintf( stderr, "Error: Invalid start address= %lld or max block addr= %lld!\n", startaddr, max_baddr );
		return -1;
	}

	if ( devcount < 2 || devcount >= MAXDEVS ) {
		fprintf( stderr, "Error: Invalid number of test devices %d!\n", devcount);
		return -1;
	}
	printf("Opening %d Devices: ", devcount);
	for (i = 0; i < devcount; i++) {

		printf("%s%s", devname[i], i < devcount-1 ? ", " : "\n");

#ifdef USE_DIRECT_IO
		/* open block device to test, warn of deleting data? */
		if ( devname[i] == NULL || (devfd[i] = open(devname[i], O_RDONLY | O_DIRECT)) < 0 ) {
			fprintf( stderr, "\nError opening device %s, error: %s!\n", devname[i], strerror(errno) );
			return -1;
		}
#else
#error Not implemented yet!
#endif
		
		if (posix_memalign ((void **)&(readbufs[i]), 4096, BLOCKSIZE) != 0) {
			fprintf( stderr, "Unable to alloc memory for read buffer, error: %s\n", strerror(errno) );
			return -1;
		}
		rd_usecs[i] = 0;
	}

	for (cycle = 0; cycle < cycles; cycle++) {

		unsigned long long blkidx;

		printf("Starting cycle %d of %d on blocks %lld - %lld, with block size: %d\n",
				cycle+1, cycles, startaddr, max_baddr, BLOCKSIZE );

		/* ATTENTION: We always use a sequential read access pattern, because we want to capture any
			*            integrity errors that are dependent on the ordering of reads (e.g. in read caches)...
			*            Changing the access pattern for reads could trigger detection of such errors... */
		for (blkidx = startaddr; blkidx < max_baddr; blkidx++) {

			int retries = 0;

			if ( read_order == access_sequential ) { /* if sequential write access pattern... */
				bn = blkidx;

			} else { /* non-sequential access pattern... get block num... */

				bn = get_block_addr_from_idx( blkidx, max_baddr, read_order );
			}
			
retry_block_check:
			for (i = 0; i < devcount; i++) {

				memset( readbufs[i], 0, BLOCKSIZE );

				start_timer();

				//fprintf(stderr, "dev: %d, read block %lld fd: %d\n", i, bn, devfd[i] );
				if ( !(err = read_block( devfd[i], readbufs[i], bn, BLOCKSIZE) ) ) {
					printf("//repcheck:dev='%s',staddr=%lld,cycles=%d/%d,mbrange=%lld,bsize=%d,"
							"blkno=%lld/%lld,rusec=%lld,errcount=1,error='%s'\n",
							devname[i], startaddr, cycle, cycles, mbrange, BLOCKSIZE,
							bn, max_baddr, rd_usecs[i], "Error reading block data buffer" );
					fprintf( stderr, "Error reading data buffer of device %s at block %lld!\n",
						devname[i], bn);
					return -1;
				} else if (err == 2) { /* EOF */
					fprintf( stderr, "Error reading data block %lld from device %s -> got EOF!\n",
							bn, devname[i]);
					return -1;
				}
				
				rd_usecs[i] += stop_timer_get_usec();
			}

			for (i = 0; i < devcount-1; i++) {

				if ( memcmp( readbufs[i], readbufs[i+1], BLOCKSIZE ) != 0 ) {
					int b = 0;

					/* OK, find the exact place where buffers differ (first byte is ok)... */
					for (b = 0; b < BLOCKSIZE; b++)
						if ( readbufs[i][b] != readbufs[i+1][b] )
							break;

					if ( b >= BLOCKSIZE ) { /* sanity check... buggy memcmp? */
						fprintf( stderr, "INTERNAL ERROR IN DATA FIDELITY TEST: [Cycle: %d] "
							"Failure @ block: %lld Devs: %s vs. %s "
							"(Block byte addr: %lld) =>> b=%d >= %d !!\n",
							cycle, bn, devname[i], devname[i+1],
							(unsigned long long)bn*BLOCKSIZE, b, BLOCKSIZE);
						return -1;
					}

					fprintf( stderr, "DATA FIDELITY TEST: [Cycle: %d] "
							"Failure @ block: %lld Devs: %s vs. %s "
							"(Block byte addr: %lld - First Byte: %d of %d) !\n",
							cycle, bn, devname[i], devname[i+1],
							(unsigned long long)bn*BLOCKSIZE, b, BLOCKSIZE);

					/* In case of failure, retry X times... */
					if ( retries++ < 3 ) {
					
						fprintf( stderr, "\nRetrying Data Check @ Block %lld (Retry No. %d)...\n\n", bn, retries );
						sleep(5);

						goto retry_block_check;
					}

					if ( b < BLOCKSIZE-16 ) {
						fprintf( stderr, ">>> EXPECT_BUF[%d]: 0x%x 0x%x 0x%x 0x%x   ---  READ_BUF[%d]: 0x%x 0x%x 0x%x 0x%x !\n\n", b,
							*((int *)&(readbufs[i][b])), *((int *)&(readbufs[i][b+4])),
							*((int *)&(readbufs[i][b+8])), *((int *)&(readbufs[i][b+12])), b,
							*((int *)&(readbufs[i+1][b])), *((int *)&(readbufs[i+1][b+4])),
							*((int *)&(readbufs[i+1][b+8])), *((int *)&(readbufs[i+1][b+12])) );
					} else {
						fprintf( stderr, ">>> EXPECT_BUF[%d]: %c ---  READ_BUF[%d]: %c !\n\n",
							b, readbufs[i][b], b, readbufs[i+1][b] );
					}

					retries = 0;
					errcount++;
					if (errcount > 3) {
						fprintf( stderr, "%s: Too many failures (3)... exiting...\n", argv[0]);
						printf("//repcheck:devcount=%d,devs='%s/%s',staddr=%lld,cycles=%d/%d,mbrange=%lld,bsize=%d,"
								"blkno=%lld/%lld,rusec=%lld,errcount=%d,error='%s'\n", devcount,
								devname[i], devname[i+1], startaddr, cycle, cycles, mbrange,
								BLOCKSIZE, bn, max_baddr, rd_usecs[i], errcount,
								"Too many (3) read verification failures" );
						return -1;
					}
				}

			}

		}

		if (errcount > 0 ) {
			printf("//repcheck:staddr=%lld,cycles=%d/%d,mbrange=%lld,bsize=%d,"
					"blkno=%lld/%lld,errcount=%d,error='%s'\n",
					startaddr, cycle, cycles, mbrange,
					BLOCKSIZE, bn, max_baddr, errcount, "Read verification failed" );
			fprintf( stderr, "%s: Read verification failed... exiting...\n", argv[0]);
			return -1;
		}

	}

	print_time_results( "Replica Checks", cycles, devcount, rd_usecs );

	printf("All replica verification cycles (%d) completed OK for %d devs: ", devcount, cycles );
	for (i = 0; i < devcount; i++)
		printf("%s%s", devname[i], i < devcount-1 ? "," : "\n");
	printf("//repcheck:devcount=%d,devs=", devcount);
	for (i = 0; i < devcount; i++)
		printf("%s%s", devname[i], i < devcount-1 ? ";" : ",");
	printf("staddr=%lld,cycles=%d/%d,mbrange=%lld,bsize=%d,"
		   "blkno=%lld/%lld,rusec=%lld,errcount=0,error='%s'\n",
			startaddr, cycle, cycles, mbrange, BLOCKSIZE, bn,
			max_baddr, rd_usecs[i], "OK" );

	return 0;
}

