
DCCEXE=dcc
DCCSTATIC=dcc.static
DCCSRC=dcc

BTOREXE=btor
BTORSRC=btorture

RECEXE=repcheck
RECSTATIC=repcheck.static
RECSRC=repcheck

all: $(DCCEXE) $(BTOREXE) $(RECEXE)

$(DCCEXE)::
	(cd $(DCCSRC) ; make $(TARGET))
	# cp $(DCCSRC)/$(DCCSTATIC) $(DCCSRC)/$(DCCEXE) .

$(BTOREXE)::
	(cd $(BTORSRC) ; make $(TARGET))

$(RECEXE)::
	(cd $(RECSRC) ; make $(TARGET))

.PHONY: clean install

clean:
	(cd $(DCCSRC) ; make $@)
	(cd $(BTORSRC) ; make $@)
	(cd $(RECSRC) ; make $@)

install:
	(cd $(DCCSRC) ; make $@)
	(cd $(BTORSRC) ; make $@)
	(cd $(RECSRC) ; make $@)


