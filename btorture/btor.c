
/* Original work Copyright (c) 2011, Michail Flouris <michail@flouris.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Rambler BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include <getopt.h>

#define BLOCKSIZE		65536
#define MIN_BLOCKSIZE	120
#define INC_BLOCKSIZE	1000
#define MAX_BLOCKSIZE	5000
#define MAX_BLOCKNO		5

struct timeval glob_start_tv;
unsigned glob_ts = 0x1007dead; /* NEED NON-ZERO RANDOM DEFAULT VALUE ! */

/* ********************************************
 *         RANDOM NUMBER GENERATOR
 * ******************************************** */

void
init_random_generator() {

	struct timeval  tv;
    struct timezone tz;

	/* Set a random seed from the current time (usecs) ... */
    if (gettimeofday( &tv, &tz) == -1) {
        fprintf( stderr, "Error in gettimeofday\n");
        exit(203);
    }
    seed48( (unsigned short *) ( ((unsigned char *) &(tv.tv_usec)) + sizeof(short)) );

}

	/* Generates & returns a random number from 0 to "max-1" value ... */
int
get_random_number( int max ) {

	 return ( lrand48() % max );
			 
}

/* ________________________________________________________________________________ */

/* Write a whole block of data (no offset in the block) */
int
write_buf( int devfd, char *data, unsigned long long boff, int bsize )
{
	int wbytes, bytes_written = 0, unwritten_bytes;

	assert( devfd > 0 && bsize > 0 );
	assert( data != NULL );

	//fprintf( stderr,"Writing %uL bytes @ offset: %uL", bsize, boff );

	for ( unwritten_bytes = (int)bsize; unwritten_bytes > 0; unwritten_bytes -= bytes_written ) {

        //fprintf( stderr,"unwritten_bytes: %d offset: %uL", unwritten_bytes, boff);

        wbytes = pwrite( devfd, data+bytes_written, (size_t)unwritten_bytes, boff+(unsigned long long)bytes_written);

        if (wbytes < 0) {
			fprintf( stderr,"Error writing to offset %llu -> pwrite() error: %s\n", boff, strerror(errno) );
			//fprintf( stderr,"DEBUG: fd: %d, unwritten: %d, offset: %llu, written: %d, bsize: %llu\n",
			//				devfd, unwritten_bytes, boff, bytes_written, bsize);
			return 0;
        }

        bytes_written += wbytes;
    }

    assert ( unwritten_bytes == 0 );
	return 1;
}

/* ________________________________________________________________________________ */

/* Read a whole block of data (no offset in the block) */
int
read_buf( int devfd, char *data, unsigned long long boff, int bsize )
{
	int wbytes, bytes_read = 0, unread_bytes;

	assert( devfd > 0 && bsize > 0 );
	assert( data != NULL );

	//fprintf( stderr,"Reading %uL bytes @ offset: %uL", bsize, boff );

	for ( unread_bytes = (int)bsize; unread_bytes > 0; unread_bytes -= bytes_read ) {

        //fprintf( stderr,"unread_bytes: %d offset: %uL", unread_bytes, boff);

        wbytes = pread( devfd, data+bytes_read, (size_t)unread_bytes, boff+(unsigned long long)bytes_read);

        if (wbytes < 0) {
			fprintf( stderr,"Error reading from offset %llu -> pread() error: %s\n", boff, strerror(errno) );
			//fprintf( stderr,"DEBUG: fd: %d, unread: %d, offset: %llu, read: %d, bsize: %llu\n",
			//				devfd, unread_bytes, boff, bytes_read, bsize);
			return 0;
        }

        bytes_read += wbytes;
    }

    assert ( unread_bytes == 0 );
	return 1;
}

/******************************************************************/
void print_usage( char *exename )
{
	printf("Data Block Torture v.0.1\n");
	printf("Usage: %s <options> <device path>\n", exename);
	printf("Options: [-c <cycle no> | --cycles <cycle no>]\n         [-m <max block no> | --mbrange <addr range (MBytes)>]\n");
	printf("         [-v | --verbose]\n");
	printf("Example: %s -c 1 -m 100 /dev/null\n", exename);
	printf("WARNING: THIS TEST WILL OVERWRITE ANY DATA ON THE DEVICE !!\n");
}

/* ________________________________________________________________________________ */

int
main(int argc, char **argv)
{
	int i, devfd, verbose = 0, cycles = 1, cycle, bs;
	int wr_passes = 0, rd_passes = 0;
	unsigned long long bn, mbrange = 1, max_baddr = 0, boff;
	char *expected_data_block, *real_data_block;

	/* Deal with arguments:
	 * <device path> <# cycles> <# write passes> <# read passes> <max block number> 
	 */
	char * devname = NULL;

	/*________ start of option processing... _________ */
	int option_index = 0;

	int c;

	static struct option long_options[] = {
		{"cycles", 1, 0, 0},
		{"wrpasses", 1, 0, 0},
		{"rdpasses", 1, 0, 0},
		{"mbrange", 1, 0, 0},
		{"verbose", 0, 0, 0},
		{0, 0, 0, 0}
	};

	if ( argc < 2 )
		goto error_parsing_options;

	while ( (c = getopt_long (argc, argv, "c:w:r:m:v",
                        long_options, &option_index) ) != -1 ) {

		//int this_option_optind = optind ? optind : 1;
		int option_index = 0;

		switch (c) {
		case 0:
			if ( strncmp(long_options[option_index].name,"cycles",7) == 0 )
				goto cycles_option;
			else if ( strncmp(long_options[option_index].name,"wrpasses",9) == 0 )
				goto wrpasses_option;
			else if ( strncmp(long_options[option_index].name,"rdpasses",9) == 0 )
				goto rdpasses_option;
			else if ( strncmp(long_options[option_index].name,"verbose",8) == 0 )
				goto verbose_option;
			else if ( strncmp(long_options[option_index].name,"mbrange",8) == 0 )
				goto mbrange_option;
		break;

		case 'c':
cycles_option:
			//printf ("Setting cycles = %s\n", optarg);
			if ( sscanf( optarg, "%d", &i ) != 1 )
				goto error_parsing_options;
			cycles = i;
		break;

		case 'w':
wrpasses_option:
			//printf ("Setting write passes = %s\n", optarg);
			if ( sscanf( optarg, "%d", &i ) != 1 )
				goto error_parsing_options;
			wr_passes = i;
		break;

		case 'r':
rdpasses_option:
			//printf ("Setting read passes = %s\n", optarg);
			if ( sscanf( optarg, "%d", &i ) != 1 )
				goto error_parsing_options;
			rd_passes = i;
		break;

		case 'm':
mbrange_option:
			//printf ("Setting max block addr to check = %s\n", optarg);
			if ( sscanf( optarg, "%lld", &bn ) != 1 )
				goto error_parsing_options;
			mbrange = bn;
		break;

		case 'v':
			// printf ("option v\n");
verbose_option:
			verbose = 1;
		break;

		default:
			printf ("Unknown option(s)... (character code 0%o ?). Aborting...\n", c);
error_parsing_options:
			print_usage(argv[0]);
			exit(-1);
		}
	} /* while... */

	/* ok, now get arguments not in the options... */
	if (optind < argc) {
		//printf ("non-option ARGV-elements: ");
		while (optind < argc) {
			char * noptarg = argv[optind++];
			//printf ("%s ", noptarg);

			/* we need just one (first) argument as device! */
			if (devname == NULL) {
				assert( strlen(noptarg) < 64 );
				devname = malloc( strlen(noptarg)+1 );
				memset( devname, 0, strlen(noptarg)+1 );
				sprintf( devname, "%s", noptarg );
			} else {
				printf ("Invalid argument: %s. Aborting...\n", noptarg);
				print_usage(argv[0]);
				exit(-1);
			}
		}
   	}
	/*________ end of option processing... _________ */

	max_baddr = (mbrange * (1024*1024)) / BLOCKSIZE;

	printf("Settings: cycles = %d, device = %s, mbrange = %lld MB (max block %lld)\n",
			cycles, devname, mbrange, max_baddr);
	printf("          write passes = %d, read passes = %d\n", wr_passes, rd_passes);

	if ( devname == NULL ) {
		fprintf( stderr, "Error: Invalid test device (NULL)!\n");
		return -1;
	}
	expected_data_block = malloc( BLOCKSIZE );
	real_data_block = malloc( BLOCKSIZE );

	/*
	 * Note: don't need to use DIRECT I/O, because we want to run with larger block sizes...
	 * FIXME: add option to use Direct I/O...
	 */

	/* open block device to test, warn of deleting data? */
	if ( devname == NULL || (devfd = open(devname, O_RDWR)) < 0 ) {
		fprintf( stderr, "Error opening test device %s, error: %s!\n", devname, strerror(errno) );
		printf("//btor:dev='%s',cycles=%d,mbrange=%lld,errcount=1,error='%s'\n",
				devname, cycles, mbrange, "Error opening device" );
		return -1;
	}

	for (cycle = 0; cycle < cycles; cycle++) {

		printf("Starting cycle %d of %d on blocks 0 - %lld, with max block size: %d\n",
				cycle+1, cycles, max_baddr, BLOCKSIZE );

#if 1
		if ( max_baddr > 0 ) { /* TEST 0 */

			if ( wr_passes > 0 ) {

				printf("Starting test 0 write pass on blocks 0 - %lld, with block size: %d\n", max_baddr, BLOCKSIZE );
			
				for (bn = 0; bn < MAX_BLOCKNO; bn++) {
					unsigned char c = '1';

					memset( expected_data_block, 0, BLOCKSIZE );

					for (i = 0, bs = MIN_BLOCKSIZE; bs < MAX_BLOCKSIZE && i + bs < MAX_BLOCKSIZE; i += bs, bs += INC_BLOCKSIZE ) {
					
						printf("Buf set: i= %d, c = %d, bs = %d\n", i, c, bs );

						memset( (char *)&expected_data_block[i], (int)c, bs );
						c++;

						boff = (unsigned long long)(bn * BLOCKSIZE) + i;
						if ( !write_buf( devfd, (char *)&expected_data_block[i], boff, bs ) ) {
							fprintf( stderr, "Error writing data buffer to offset %lld!\n", boff);
							printf("//btor:dev='%s',cycles=%d,mbrange=%lld,bsize=%d,blkno=%lld,errcount=1,error='%s'\n",
									devname, cycles, mbrange, bs, bn, "Error writing data buffer" );
							return -1;
						}

						fdatasync( devfd );
						fsync( devfd );
					}

				}
			}

			/* ensure all data on stable storage */
			fdatasync( devfd );
			fsync( devfd );

			if ( rd_passes > 0 ) {

				printf("Starting test 0 READ pass on blocks 0 - %lld, with block size: %d\n", max_baddr, BLOCKSIZE );

				for (bn = 0; bn < MAX_BLOCKNO; bn++) {
					unsigned char c = '1';

					memset( real_data_block, 0, BLOCKSIZE );
					memset( expected_data_block, 0, BLOCKSIZE );

					for (i = 0, bs = MIN_BLOCKSIZE; bs < MAX_BLOCKSIZE && i + bs < MAX_BLOCKSIZE; i += bs, bs += INC_BLOCKSIZE ) {
					
						printf("Buf set: i= %d, c = %d, bs = %d\n", i, c, bs );

						memset( (char *)&expected_data_block[i], (int)c, bs );
						c++;
					}
					bs -= INC_BLOCKSIZE;

					boff = (unsigned long long)(bn * BLOCKSIZE);
					if ( !read_buf( devfd, real_data_block, boff, bs ) ) {
						fprintf( stderr, "Error reading data buffer from offset %lld!\n", boff);
						printf("//btor:dev='%s',cycles=%d,mbrange=%lld,bsize=%d,blkno=%lld,errcount=1,error='%s'\n",
								devname, cycles, mbrange, bs, bn, "Error reading data buffer" );
						return -1;
					}

					if ( memcmp( expected_data_block, real_data_block, bs ) != 0 ) {
						int b = 0;

						/* OK, find the exact place where buffers differ (first byte is ok)... */
						for (b = 0; b < BLOCKSIZE; b++)
							if ( expected_data_block[b] != real_data_block[b] )
								break;

						assert ( b < BLOCKSIZE ); /* sanity check... buggy memcmp? */
						fprintf( stderr, "DATA FIDELITY TEST: Failure @ block: %lld offset:%lld (Block byte addr: %lld - First Error Byte: %d of %d) !\n",
								bn, boff, (unsigned long long)bn*BLOCKSIZE, b, bs);

						printf("//btor:dev='%s',cycles=%d,mbrange=%lld,bsize=%d,blkno=%lld,errcount=1,error='%s'\n",
								devname, cycles, mbrange, bs, bn, "Read verification failed" );
						fprintf( stderr, "%s: Read verification failed... exiting...\n", argv[0]);
						return -1;
					}
				}
			}

			printf("Test 0 completed OK!\n");
		}
#endif

	}

	printf("All %d data verification cycles completed OK!\n", cycles );
	printf("//btor:dev='%s',cycles=%d,mbrange=%lld,errcount=1,error='%s'\n", devname, cycles, mbrange, "OK" );

	return 0;
}
