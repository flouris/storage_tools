
/* Original work Copyright (c) 2011, Michail Flouris <michail@flouris.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Rambler BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Pending features (Updated Feb 2013, by Michail):
 *
 * FIXME: improve unique block generation... generate 4 unique bytes per 512 sector
 *        in that block, not per whole block...
 *
 * - cmd-line option to select the buffer block size
 * - cmd-line direct I/O option and handling code
 * - measurements for throughput during each pass & cycle + reporting
 * - random read and check passes
 * - option for multi-threaded write & read operation
 * - max time-limit per pass option
 * - random write pass with modified bufs & recheck [more complex check patterns]
 * - max memory limit option
 *
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include <getopt.h>
#include <stdbool.h>

#define DBPOOLSIZE 512

// FIXME: direct I/O support is not complete! need block alignment!
#undef USE_DIRECT_IO

#ifdef USE_DIRECT_IO
#define BLOCKSIZE 65536
#else
#define BLOCKSIZE 65000
#endif

#undef ENABLE_DEBUG

struct timeval glob_start_tv;
unsigned glob_ts = 0x1007dead; /* NEED NON-ZERO RANDOM DEFAULT VALUE ! */

/* ********************************************
 *         RANDOM NUMBER GENERATOR
 * ******************************************** */

void
init_random_generator() {

	struct timeval  tv;
    struct timezone tz;

	/* Set a random seed from the current time (usecs) ... */
    if (gettimeofday( &tv, &tz) == -1) {
        fprintf( stderr, "Error in gettimeofday\n");
        exit(203);
    }
    seed48( (unsigned short *) ( ((unsigned char *) &(tv.tv_usec)) + sizeof(short)) );

}

	/* Generates & returns a random number from 0 to "max-1" value ... */
int
get_random_number( int max ) {

	 return ( lrand48() % max );
			 
}

static unsigned
stirred_bits( unsigned seed )
{
	seed ^= seed << 3;
	seed += seed >> 5;
	seed ^= seed << 4;
	seed += seed >> 17;
	seed ^= seed << 25;
	seed += seed >> 6;
	return seed;
}

static unsigned long long
stirred_ll_bits( unsigned long long seed )
{
	seed ^= seed << 8;
	seed += seed >> 5;
	seed ^= seed << 14;
	seed += seed >> 17;
	seed ^= seed << 35;
	seed += seed >> 12;
	return seed;
}

/* _______________________________________________________________________________*/
/* Generate a data buffer of size "bufsize", based on the given random buffer set,
 * but different (unique?) for a specific block number (address) */
int
gen_block_data_buffer( unsigned long long pattern_seed, char **rndbufs, int bufpoolsize,
						int bufsize, unsigned long long blockno, char *outbuf )
{
	char *randbuf;
	unsigned mixp1, mixp2, mixp3, p1, p2;
	unsigned long long mixd1, mixd2, mixd3;

	//fprintf( stderr, "ENTER gen_block_data_buffer for block: %lld\n", blockno);
	randbuf = rndbufs[ (int)(blockno % bufpoolsize) ];

	// FIXME: improve unique block generation... generate 4 unique bytes per 512 sector in that block...

	if (blockno > 0) {

		p1 = (unsigned) blockno;
		p2 = (unsigned) stirred_bits((blockno ^ 0xfeedaa00) == 0 ? 0xdeadbee : (blockno ^ 0xfeedaa00));

		mixd1 = (unsigned long long)glob_ts + (unsigned long long)p1;
		mixd2 = stirred_ll_bits( (unsigned long long)glob_ts + ~p1 );
		mixd3 = stirred_ll_bits( (unsigned long long)glob_ts + p2 );
		assert( mixd1 && mixd2 && mixd3 );

		//mixp1 = glob_ts % (bufsize - sizeof(unsigned));
		mixp1 = 0;
		mixp2 = blockno % (bufsize - sizeof(unsigned long long));
		mixp3 = (glob_ts ^ blockno) % (bufsize - sizeof(unsigned long long));
		assert( mixp1 >= 0 && mixp2 >= 0 && mixp3 >= 0 );
		assert( mixp1 < bufsize && mixp2 < bufsize && mixp3 < bufsize );

#ifdef ENABLE_DEBUG
		/* Debug... */
		printf("pattern_seed: %llu, blockno= %llu, p1: %u, p2= %u\n",
				pattern_seed, blockno, p1, p2);
		printf("[mixp1: %u, mixd1= %llu][mixp2: %u, mixd2= %llu][mixp3: %u, mixd3= %llu]\n",
				mixp1, mixd1,mixp2, mixd2, mixp3, mixd3);
#endif

		memcpy( outbuf, randbuf, bufsize );
		memcpy( (char *)(outbuf + mixp1), (char *)&mixd1, sizeof(unsigned long long) );
		memcpy( (char *)(outbuf + mixp2), (char *)&mixd2, sizeof(unsigned long long) );
		memcpy( (char *)(outbuf + mixp3), (char *)&mixd3, sizeof(unsigned long long) );

	} else {
		memcpy( outbuf, randbuf, bufsize );
	}

	return 0;
}

struct entry {
	unsigned long long seed;
	unsigned long long block;
	unsigned long pass;
} __attribute__((packed));

/**
 * Populates the specified buffer using the seed, the block number, and the
 * pass number.
 */
int gen_block_data_buffer2(const unsigned long long seed,
	const unsigned long long blk, const unsigned int pass, const size_t size,
	const char *buf) {

	size_t left = size;
	struct entry *entry = (struct entry*)buf;

	while (left >= sizeof(struct entry)) {
		entry->seed = seed;
		entry->block = blk;
		entry->pass = pass;
		entry++;
		left -= sizeof(struct entry);
	}

	memset(entry, 0, (size_t)buf + size - (size_t)entry);

	return 0;
}

void show_entry_diffs(const struct entry * const expected,
	const struct entry * const actual, const size_t off) {

	if (expected->seed != actual->seed)
		fprintf(stderr, "offset %lu: expected seed %llu but got %llu\n", off,
			expected->seed, actual->seed);
	if (expected->block != actual->block)
		fprintf(stderr, "offset %lu: expected block %llu but got %llu\n", off,
			expected->block, actual->block);
	if (expected->pass != actual->pass)
		fprintf(stderr, "offset %lu: expected pass %lu but got %lu\n", off,
			expected->pass, actual->pass);
}

void show_block_diffs(const char * const expected, const char * const actual,
	const size_t size) {

	struct entry *exp_entry = (struct entry*)expected,
		*act_entry = (struct entry*)actual;
	size_t left = size, off = 0;

	assert(expected);
	assert(actual);
	assert(size);

	while (left >= sizeof(struct entry)) {
		show_entry_diffs(exp_entry, act_entry, off++);
		exp_entry++;
		act_entry++;
		left -= sizeof(struct entry);
	}

	for (off = (size_t)exp_entry; off < size; off++)
		if (expected[off] != actual[off])
			fprintf(stderr, "trailing byte %lu differs (%u != %u)\n", off,
				expected[off], actual[off]);
}

/* ________________________________________________________________________________ */

/* Write a whole block of data (no offset in the block) */
int
write_block( int devfd, char *data, unsigned long long blockno, int bsize )
{
	int wbytes, bytes_written = 0, unwritten_bytes, offset = 0;
	unsigned long long boff = blockno * (unsigned long long)bsize + offset;

	assert( devfd > 0 && bsize > 0 );
	assert( data != NULL );

	//fprintf( stderr,"Writing %uL bytes @ offset: %uL [Block No: %uL, Offset: %uL]", bsize, boff, blockno, offset);

	for ( unwritten_bytes = (int)bsize; unwritten_bytes > 0; unwritten_bytes -= wbytes ) {

        //fprintf( stderr,"unwritten_bytes: %d offset: %uL", unwritten_bytes, boff);

        wbytes = pwrite( devfd, data+bytes_written, (size_t)unwritten_bytes, boff+(unsigned long long)bytes_written);

        if (wbytes < 0) {
			fprintf( stderr,"Error writing to offset %llu -> pwrite() error: %s\n", boff, strerror(errno) );
#ifdef ENABLE_DEBUG
			fprintf( stderr,"DEBUG: fd: %d, unwritten: %d, offset: %llu, written: %d, bsize: %llu\n",
							devfd, unwritten_bytes, boff, bytes_written, bsize);
#endif
			return 0;
        }
		else if (wbytes == 0) {
			fprintf( stderr,"Error: ZERO data writing to offset %llu -> pwrite() error: %s\n", boff, strerror(errno) );
			return 2; /* No data written! */
		}

        bytes_written += wbytes;
    }

    assert ( unwritten_bytes == 0 );
	return 1;
}

/* ________________________________________________________________________________ */

/* Read a whole block of data (no offset in the block) */
int
read_block( int devfd, char *data, unsigned long long blockno, int bsize )
{
	int rbytes, bytes_read = 0, unread_bytes, offset = 0;
	unsigned long long boff = blockno * (unsigned long long)bsize + offset;

	assert( devfd > 0 && bsize > 0 );
	assert( data != NULL );

	//fprintf( stderr,"Reading %uL bytes @ offset: %uL [Block No: %uL, Offset: %uL]", bsize, boff, blockno, offset);

	for ( unread_bytes = (int)bsize; unread_bytes > 0; unread_bytes -= rbytes ) {

        //fprintf( stderr,"unread_bytes: %d offset: %uL", unread_bytes, boff);

        rbytes = pread( devfd, data+bytes_read, (size_t)unread_bytes, boff+(unsigned long long)bytes_read);

        if (rbytes < 0) {
			fprintf( stderr,"Error reading from offset %llu -> pread() error: %s\n", boff, strerror(errno) );
#ifdef ENABLE_DEBUG
			fprintf( stderr,"DEBUG: fd: %d, unread: %d, offset: %llu, read: %d, bsize: %llu\n",
							devfd, unread_bytes, boff, bytes_read, bsize);
#endif
			return 0;
        }
		else if (rbytes == 0) {
			fprintf( stderr,"Error reading (EOF) at offset %llu -> pread() error: %s\n", boff, strerror(errno) );
			return 2; /* EOF */
		}

        bytes_read += rbytes;
    }

    assert ( unread_bytes == 0 );
	return 1;
}

typedef enum {
	access_sequential = 1,
	access_reverse_seq = 2,
	access_random_chunks = 3,
}	write_order_t;

unsigned chunk_size_blocks = 60;
unsigned long long	*rearr_chunk_tbl = NULL, rearr_chunk_cnt;

const char *
write_order_to_string( write_order_t write_order)
{
	switch (write_order) {
	case access_random_chunks:
		return "random chunks";
	case access_sequential:
		return "sequential access";
	case access_reverse_seq:
		return "reverse sequential";
	default:
		return "ERROR: UNKNOWN!";
	}
}

bool
swap_chunks( unsigned long long	*rearr_chunk_tbl,
			 unsigned long long rearr_chunk_cnt,
			 unsigned first, unsigned second )
{
	unsigned long long tmp;

	if (first < 0 || second < 0 ||
		first >= rearr_chunk_cnt || second >= rearr_chunk_cnt)
		return false;
	
	assert( rearr_chunk_tbl[first] >= 0 && rearr_chunk_tbl[first] < rearr_chunk_cnt );
	assert( rearr_chunk_tbl[second] >= 0 && rearr_chunk_tbl[second] < rearr_chunk_cnt );
	tmp = rearr_chunk_tbl[first];
	rearr_chunk_tbl[first] = rearr_chunk_tbl[second];
	rearr_chunk_tbl[second] = tmp;
#ifdef ENABLE_DEBUG
	fprintf(stderr,"tbl[%d] = %lld <-> tbl[%d] = %lld\n", first, rearr_chunk_tbl[first],
		second, rearr_chunk_tbl[second] );
#endif
	return true;
}


/* ATTENTION: this function should cover all block addresses for all indexes between
 *            0 and maxblkaddr... easiest solution would be to
 */
unsigned long long
get_block_addr_from_idx( unsigned long long blkidx,
                         unsigned long long maxblkaddr, write_order_t write_order )
{
	int i, rearr_chunk_idx;

	switch (write_order) {
	case access_random_chunks:
		/* non-sequential access pattern... it's not completely random, but mostly alternating chunks
		   from the start and end of disk block range... */
	
	/* if chunk rearrangement table does not exist, create it... */
	if ( !rearr_chunk_tbl ) {

		/* try to adjust chunk_size_blocks to a zero mod of blocks... */
		while (maxblkaddr % chunk_size_blocks && chunk_size_blocks > 7) {
			chunk_size_blocks--;
		}

		if (maxblkaddr % chunk_size_blocks) {
			write_order = access_sequential;
			return blkidx;
		}

		rearr_chunk_cnt = maxblkaddr / chunk_size_blocks;
		if ( rearr_chunk_cnt < 4 ) { /* for very small sizes, just do sequential... */
			write_order = access_sequential;
			return blkidx;
		}

		rearr_chunk_tbl = (unsigned long long *) malloc( rearr_chunk_cnt * sizeof(*rearr_chunk_tbl) );
		if ( !rearr_chunk_tbl ) {
			fprintf( stderr, "Unable to alloc memory for chunk_tbl, error: %s\n", strerror(errno) );
			exit(EXIT_FAILURE);
		}
		memset( rearr_chunk_tbl, 0, rearr_chunk_cnt * sizeof(*rearr_chunk_tbl));

		for (i = 0; i < rearr_chunk_cnt; i++ )
			rearr_chunk_tbl[i] = i;

		/* NOTE: swap chunks around the center of the address space...
		 *
		 * ATTENTION: to get reproducible pattern, one must use THE SAME input block range (address space)!
		 */
		for (i = 0; i < rearr_chunk_cnt / 2; i+=2 )
			swap_chunks( rearr_chunk_tbl, rearr_chunk_cnt, i, rearr_chunk_cnt-i-1);
	}

	rearr_chunk_idx = blkidx / chunk_size_blocks;
	if (rearr_chunk_idx >= 0 && rearr_chunk_idx < rearr_chunk_cnt) {
		assert (rearr_chunk_tbl[rearr_chunk_idx] >= 0 &&
				rearr_chunk_tbl[rearr_chunk_idx] < rearr_chunk_cnt );
		return (rearr_chunk_tbl[rearr_chunk_idx] * chunk_size_blocks) + (blkidx % chunk_size_blocks);
	}

	break;
	case access_sequential:
		return blkidx;
	case access_reverse_seq:
		return (maxblkaddr - 1 - blkidx) >= 0 ? (maxblkaddr - 1 - blkidx) : blkidx;
	default:
		assert(write_order >= 1 && write_order <= 3);
	break;
	}

	return blkidx;
}

static struct timeval  gtv1, gtv2;
static struct timezone gtz;

/****************  START TIMER   **********************************/
int
start_timer( void )
{
	if (gettimeofday( &gtv1, &gtz) == -1) {
		fprintf( stderr, "Error in gettimeofday (start_timer)\n");
		return 0;
	}
	return 1;
}
/******************************************************************/

int
stop_timer( void )
{
	if (gettimeofday( &gtv2, &gtz) == -1) {
		fprintf( stderr, "Error in gettimeofday (stop_timer)\n");
		return 0;
	}
	return 1;
}
/******************************************************************/

void
print_results( char * testid, int reps, long long *rusecs )
{
	long long elapsed_usecs;
	double  elapsed_time;

	elapsed_usecs = ((long long)(gtv2.tv_sec - gtv1.tv_sec)) * 1000000LL +
					((long long)(gtv2.tv_usec - gtv1.tv_usec));
	assert( elapsed_usecs > 0 );

	elapsed_time = (((double) (gtv2.tv_sec - gtv1.tv_sec) * 1000000.0) +
					((double) (gtv2.tv_usec - gtv1.tv_usec))) / (double) 1000000.0;

	*rusecs = elapsed_usecs;
	printf("\n\"%s\" => TOTAL TIME    : %.6f secs, %lld usecs.\n", testid, elapsed_time, elapsed_usecs );
	printf(  "\"%s\" => AVG TIME/CALL : %.6f secs, %lld usecs.\n", testid,
						elapsed_time / (double)reps, elapsed_usecs / reps );
	printf("====================================================================================\n");
}

/******************************************************************/
void print_usage( char *exename )
{
	printf("Data Check Cycle v.0.1\n");
	printf("Usage: %s <options> <device path>\n", exename);
	printf("Options: [-c <cycle no> | --cycles <cycle no>]\n         [-m <data size (MB)> | --mbrange <data size (MBytes)>]\n");
	printf("         [-w <# write passes> | --wrpasses <# write passes>]\n         [-r <# read passes> | --rdpasses <# read passes>]\n");
	printf("         [-p <64-bit pattern> | --pattern <64-bit pattern seed>]\n         [-v | --verbose]\n");
	printf("         [-n | --bnpattern]\n         [-o | --writeorder <order: 1=sequential,2=reverse_sequential,3:random_chunks>]\n");
	printf("Example: %s -w 1 -r 0 -m 100 -p 0x00792338deadbabe1 /dev/null\n", exename);
	printf("WARNING: Write Cycles WILL OVERWRITE ANY DATA ON THE DEVICE !!\n");
}

/* ________________________________________________________________________________ */

int
main(int argc, char **argv)
{
	int i, randfd = -1, devfd, verbose = 0, errcount = 0, err;
	int cycles = 1, wr_passes = 0, rd_passes = 0, cycle, wrp = 0, rrp = 0;
	unsigned long long bn, mbrange = 1024, max_baddr = 0, pattern_seed = 0;
	long long wr_usecs = 0, rd_usecs = 0;
	char *expected_data_block, *real_data_block;
	char *rndbufs[DBPOOLSIZE];
	bool use_bnpattern = false;
	write_order_t write_order = access_random_chunks;

	/* Deal with arguments: <device path> <# cycles> <# write passes> <# read passes> <max block number> */
	char * devname = NULL;

	/*________ start of option processing... _________ */
	int option_index = 0;

	int c;

	static struct option long_options[] = {
		{"cycles", 1, 0, 0},
		{"wrpasses", 1, 0, 0},
		{"rdpasses", 1, 0, 0},
		{"mbrange", 1, 0, 0},
		{"pattern", 1, 0, 0},
		{"writeorder", 1, 0, 0}, /* ATTENTION: default is non-sequential access! */
		{"bnpattern", 0, 0, 0},
		{"verbose", 0, 0, 0},
		{0, 0, 0, 0}
	};

	if ( argc < 2 )
		goto error_parsing_options;

	while ( (c = getopt_long (argc, argv, "c:w:r:m:p:o:vn",
                        long_options, &option_index) ) != -1 ) {

		//int this_option_optind = optind ? optind : 1;
		int option_index = 0;

		switch (c) {
		case 0:
			//printf ("option \"%s\"", long_options[option_index].name);
			//if (optarg)
			//	printf (" with arg %s", optarg);
			//printf ("\n");

			if ( strncmp(long_options[option_index].name,"cycles",7) == 0 )
				goto cycles_option;
			else if ( strncmp(long_options[option_index].name,"wrpasses",9) == 0 )
				goto wrpasses_option;
			else if ( strncmp(long_options[option_index].name,"rdpasses",9) == 0 )
				goto rdpasses_option;
			else if ( strncmp(long_options[option_index].name,"verbose",8) == 0 )
				goto verbose_option;
			else if ( strncmp(long_options[option_index].name,"mbrange",8) == 0 )
				goto mbrange_option;
			else if ( strncmp(long_options[option_index].name,"pattern",8) == 0 )
				goto pattern_option;
			else if ( strncmp(long_options[option_index].name,"bnpattern",10) == 0 )
				goto bnpattern_option;
			else if ( strncmp(long_options[option_index].name,"writeorder",10) == 0 )
				goto writeorder_option;
		break;

		case 'c':
cycles_option:
			//printf ("Setting cycles = %s\n", optarg);
			if ( sscanf( optarg, "%d", &i ) != 1 )
				goto error_parsing_options;
			cycles = i;
		break;

		case 'w':
wrpasses_option:
			//printf ("Setting write passes = %s\n", optarg);
			if ( sscanf( optarg, "%d", &i ) != 1 )
				goto error_parsing_options;
			wr_passes = i;
		break;

		case 'r':
rdpasses_option:
			//printf ("Setting read passes = %s\n", optarg);
			if ( sscanf( optarg, "%d", &i ) != 1 )
				goto error_parsing_options;
			rd_passes = i;
		break;

		case 'm':
mbrange_option:
			//printf ("Setting max block addr to check = %s\n", optarg);
			if ( sscanf( optarg, "%lld", &bn ) != 1 )
				goto error_parsing_options;
			mbrange = bn;
		break;

		case 'p':
pattern_option:
			//printf ("Setting pattern to check = %s\n", optarg);
			if ( sscanf( optarg, "%llx", &pattern_seed ) != 1 )
				goto error_parsing_options;
		break;

		case 'n':
bnpattern_option:
			/* use predictable data pattern in blocks... */
			use_bnpattern = true;
		break;

		case 'o':
writeorder_option:
			//printf ("Setting write order to = %s\n", optarg);
			if ( sscanf( optarg, "%d", &i ) != 1 )
				goto error_parsing_options;
			if (i >= 1 && i <= 3)
				write_order = i; /* set write order (IO access pattern) - default is random */
		break;

		case 'v':
verbose_option:
			verbose = 1;
		break;

		default:
			printf ("Unknown option(s)... (character code 0%o ?). Aborting...\n", c);
error_parsing_options:
			print_usage(argv[0]);
			exit(-1);
		}
	} /* while... */

	if (use_bnpattern && wr_passes > 1) {
		/*
		 * If use_bnpattern in cycle we will be writing the same pattern,
		 * independently of the current write pass, so each write pass will be
		 * writing the exact same data as the previous one. This isn't much of
		 * a test.
		 */
		fprintf(stderr, "-w cannot be greater than 1 when -n is used");
		exit(EXIT_FAILURE);
	}

	/* ok, now get arguments not in the options... */
	if (optind < argc) {
		//printf ("non-option ARGV-elements: ");
		while (optind < argc) {
			char * noptarg = argv[optind++];
			//printf ("%s ", noptarg);

			/* we need just one (first) argument as device! */
			if (devname == NULL) {
				assert( strlen(noptarg) < 64 );
				devname = malloc( strlen(noptarg)+1 );
				memset( devname, 0, strlen(noptarg)+1 );
				sprintf( devname, "%s", noptarg );
			} else {
				printf ("Invalid argument: %s. Aborting...\n", noptarg);
				print_usage(argv[0]);
				exit(-1);
			}
		}
   	}
	/*________ end of option processing... _________ */

	max_baddr = (mbrange * (1024*1024)) / BLOCKSIZE;

	if (write_order == access_random_chunks && max_baddr % chunk_size_blocks ) {
		max_baddr -= max_baddr % chunk_size_blocks;
		mbrange = (max_baddr * BLOCKSIZE) / (1024*1024);
		fprintf( stderr, "NOTE: Adjusting max block to %lld (%lld MBytes)\n", max_baddr, mbrange );
	}

	printf("Settings: cycles = %d, write passes = %d, read passes = %d write_order = %d (%s)\n",
				cycles, wr_passes, rd_passes, write_order, write_order_to_string( write_order ) );
	printf("  device = %s, mbrange = %lld MB (max block %lld), pattern = 0x%llx, bnpattern = %s\n",
				devname, mbrange, max_baddr, pattern_seed, use_bnpattern ? "on" : "off");

	if ( devname == NULL ) {
		fprintf( stderr, "Error: Invalid test device (NULL)!\n");
		return -1;
	}

	for (i = 0; i < DBPOOLSIZE; i++)
		if ( !(rndbufs[i] = malloc( BLOCKSIZE )) ) {
			fprintf( stderr, "Unable to alloc memory, error: %s\n", strerror(errno) );
			return -1;
		}
	expected_data_block = malloc( BLOCKSIZE );
	real_data_block = malloc( BLOCKSIZE );
	if ( !expected_data_block || !real_data_block ) {
		fprintf( stderr, "Unable to alloc memory, error: %s\n", strerror(errno) );
		return -1;
	}

	/*
	 * Note: don't need to use DIRECT I/O, because we want to run with larger block sizes...
	 * FIXME: add option to use Direct I/O...
	 */

	/* No pattern provided> USE UNIQUE GENERATED RANDOM PATTERN -> ATTENTION: NON-REPRODUCIBLE IN A DIFFERENT RUN! */
	if ( !pattern_seed ) {

		if ( gettimeofday( &glob_start_tv, NULL) < 0 ) {
			fprintf( stderr, "Error in gettimeofday: %s\n", strerror(errno) );
			return -1;
		}
		/* needed for unique time-dependent pattern... */
		glob_ts = glob_start_tv.tv_sec*1000000 + glob_start_tv.tv_usec;

		/* open /dev/urandom ...*/
		if ( (randfd = open("/dev/urandom", O_RDONLY)) < 0 ) {
			fprintf( stderr, "Error opening /dev/urandom: %s \n", strerror(errno) );
			return -1;
		}
	}

#ifdef USE_DIRECT_IO
	/* open block device to test, warn of deleting data? */
	if ( devname == NULL || (devfd = open(devname, O_RDWR | O_DIRECT)) < 0 ) {
		fprintf( stderr, "Error opening test device %s, error: %s!\n", devname, strerror(errno) );
		return -1;
	}
#else
	/* open block device to test, warn of deleting data? */
	if ( devname == NULL || (devfd = open(devname, O_RDWR)) < 0 ) {
		fprintf( stderr, "Error opening test device %s, error: %s!\n", devname, strerror(errno) );
		return -1;
	}
#endif
	for (cycle = 0; cycle < cycles; cycle++) {

		printf("Starting cycle %d of %d on blocks 0 - %lld, with block size: %d\n",
				cycle+1, cycles, max_baddr, BLOCKSIZE );


		if (!use_bnpattern) {

			if ( !pattern_seed ) { /* No pattern provided! USE UNIQUE GENERATED RANDOM PATTERN! */

			/* from /dev/urandom read array of random numbers and populate random block array */

				printf("Populating %d random buffers, each of size: %d\n", DBPOOLSIZE, BLOCKSIZE );

				for (i = 0; i < DBPOOLSIZE; i++) {
					if ( read( randfd, rndbufs[i], BLOCKSIZE ) < 0 ) {
						printf("//dcc:dev='%s',pat=0x%llx,cycles=%d/%d,wpass='0/%d',rpass='0/%d',mbrange=%lld,bsize=%d,"
							   "blkno=%lld/%lld,wusec=%lld,rusec=%lld,errcount=1,error='%s'\n",
								devname, pattern_seed, cycle, cycles, wr_passes, rd_passes, mbrange, BLOCKSIZE, bn,
								max_baddr, wr_usecs, rd_usecs, "Cannot read random device" );
						return -1;
					}
				}

			} else { /* Using provided pattern seed for reproducible data patterns! */
				int j;

				printf("Using pattern 0x%llx to populate %d random buffers, each of size: %d\n",
						pattern_seed, DBPOOLSIZE, BLOCKSIZE );

				for (i = 0; i < DBPOOLSIZE; i++) {

					memset( rndbufs[i], (unsigned int)i, BLOCKSIZE );
					for (j = 0; j < BLOCKSIZE; j+=sizeof(unsigned long long)) {

							*((unsigned long long *)&(rndbufs[i][j])) = stirred_ll_bits(pattern_seed + (unsigned long long)i);
					}
				}
			}
		}

		if ( wr_passes > 0 ) {

			printf("Starting %d write passes on blocks 0 - %lld, with block size: %d\n",
					wr_passes, max_baddr, BLOCKSIZE );
			
			start_timer();

			for (wrp = 0; wrp < wr_passes; wrp++) {

				unsigned long long blkidx;

				for (blkidx = 0; blkidx < max_baddr; blkidx++) {

					if ( write_order == access_sequential ) { /* if sequential write access pattern... */
						bn = blkidx;

					} else { /* non-sequential access pattern... get block num... */

						bn = get_block_addr_from_idx( blkidx, max_baddr, write_order );
					}

					if (use_bnpattern) {
						err = gen_block_data_buffer2(pattern_seed, bn, cycle,
							BLOCKSIZE, expected_data_block);
					} else {
						err = gen_block_data_buffer( pattern_seed, rndbufs,
							DBPOOLSIZE, BLOCKSIZE, bn, expected_data_block);
						if (err)
							printf("//dcc:dev='%s',pat=0x%llx,cycles=%d/%d,wpass=%d/%d,rpass='0/%d',mbrange=%lld,bsize=%d,"
								   "blkno=%lld/%lld,wusec=%lld,rusec=%lld,errcount=1,error='%s'\n",
									devname, pattern_seed, cycle, cycles, wrp, wr_passes, rd_passes, mbrange, BLOCKSIZE,
									bn, max_baddr, wr_usecs, rd_usecs, "Error generating block data buffer" );
					}

					if (err) {
						fprintf( stderr, "Error generating block data buffer\n");
						return -1;
					}

					if ( !write_block( devfd, expected_data_block, bn, BLOCKSIZE ) ) {
						printf("//dcc:dev='%s',pat=0x%llx,cycles=%d/%d,wpass=%d/%d,rpass='0/%d',mbrange=%lld,bsize=%d,"
							   "blkno=%lld/%lld,wusec=%lld,rusec=%lld,errcount=1,error='%s'\n",
								devname, pattern_seed, cycle, cycles, wrp, wr_passes, rd_passes, mbrange, BLOCKSIZE,
								bn, max_baddr, wr_usecs, rd_usecs, "Error writing data buffer" );
						fprintf( stderr, "Error writing data buffer at block %lld!\n", bn);
						return -1;
					} else if (err == 2) { /* No data written... */
						fprintf( stderr, "Error writing data block %lld - write FAILED!\n", bn);
						return -1;
					}
#ifdef ENABLE_DEBUG
					printf("BLOCK [IDX %lld / %lld ] -> %lld\n", blkidx, max_baddr, bn );
#endif
				}
				printf("Write pass %d of %d Completed OK\n", wrp + 1, wr_passes );

				/* ensure all data on stable storage */
				fdatasync( devfd );
				fsync( devfd );
			}

			stop_timer();
			print_results( "WRITE Passes", wr_passes, &wr_usecs );

			printf("All %d write passes completed OK!\n", wr_passes );

			/* ensure all data on stable storage */
			fdatasync( devfd );
			fsync( devfd );

			sleep(5);
		}

		if ( rd_passes > 0 ) {

			printf("Starting %d read passes on blocks 0 - %lld, with block size: %d\n",
					rd_passes, max_baddr, BLOCKSIZE );

			start_timer();

			for (rrp = 0; rrp < rd_passes; rrp++) {

				/* ATTENTION: We always use a sequential read access pattern, because we want to capture any
				 *            integrity errors that are dependent on the ordering of writes (e.g. in write caches)...
				 *            Changing the access pattern for reads could trigger detection of such errors... */
				for (bn = 0; bn < max_baddr; bn++) {
					
					int retries = 0;

retry_block_check:
					memset( real_data_block, 0, BLOCKSIZE );

					if (use_bnpattern)
						err = gen_block_data_buffer2(pattern_seed, bn, cycle,
							BLOCKSIZE, expected_data_block);
					else {
						err = gen_block_data_buffer(pattern_seed, rndbufs,
							DBPOOLSIZE, BLOCKSIZE, bn, expected_data_block);
						if (err)
							printf("//dcc:dev='%s',pat=0x%llx,cycles=%d/%d,wpass=%d/%d,rpass=%d/%d,mbrange=%lld,bsize=%d,"
								   "blkno=%lld/%lld,wusec=%lld,rusec=%lld,errcount=1,error='%s'\n",
									devname, pattern_seed, cycle, cycles, wrp, wr_passes, rrp, rd_passes, mbrange, BLOCKSIZE,
									bn, max_baddr, wr_usecs, rd_usecs, "Error generating block data buffer" );
					}

					if (err) {
						fprintf( stderr, "Error generating block data buffer\n");
						return -1;
					}

					if ( !(err = read_block( devfd, real_data_block, bn, BLOCKSIZE) ) ) {
						printf("//dcc:dev='%s',pat=0x%llx,cycles=%d/%d,wpass=%d/%d,rpass=%d/%d,mbrange=%lld,bsize=%d,"
							   "blkno=%lld/%lld,wusec=%lld,rusec=%lld,errcount=1,error='%s'\n",
								devname, pattern_seed, cycle, cycles, wrp, wr_passes, rrp, rd_passes, mbrange, BLOCKSIZE,
								bn, max_baddr, wr_usecs, rd_usecs, "Error reading block data buffer" );
						fprintf( stderr, "Error reading data buffer at block %lld!\n", bn);
						return -1;
					} else if (err == 2) { /* EOF */
						fprintf( stderr, "Error reading data block %lld - got EOF!\n", bn);
						return -1;
					}

					if ( memcmp( expected_data_block, real_data_block, BLOCKSIZE ) != 0 ) {
						int b = 0;


						if (use_bnpattern)
							show_block_diffs(expected_data_block,
								real_data_block, BLOCKSIZE);
						else {

							/* OK, find the exact place where buffers differ (first byte is ok)... */
							for (b = 0; b < BLOCKSIZE; b++)
								if ( expected_data_block[b] != real_data_block[b] )
									break;
	
							if ( b >= BLOCKSIZE ) { /* sanity check... buggy memcmp? */
								fprintf( stderr, "INTERNAL ERROR IN DATA FIDELITY TEST: [Pass: %d] Failure @ block: %lld "
										"(Block byte addr: %lld) =>> b=%d >= %d !!\n",
										rrp, bn, (unsigned long long)bn*BLOCKSIZE, b, BLOCKSIZE);
								return -1;
							}
	
							fprintf( stderr, "DATA FIDELITY TEST: [Pass: %d] Failure @ block: %lld "
									"(Block byte addr: %lld - First Error Byte: %d of %d) !\n",
									rrp, bn, (unsigned long long)bn*BLOCKSIZE, b, BLOCKSIZE);
						}

						/* In case of failure, retry X times... */
						if ( retries++ < 3 ) {
							
							fprintf( stderr, "\nRetrying Data Check @ Block %lld (Retry No. %d)...\n\n", bn, retries );
							memset( expected_data_block, 0, BLOCKSIZE );

							/* ensure all data on stable storage */
							fdatasync( devfd );
							fsync( devfd );

							sleep(5);
							goto retry_block_check;
						}

						if ( b < BLOCKSIZE-16 ) {
							fprintf( stderr, ">>> EXPECT_BUF[%d]: 0x%x 0x%x 0x%x 0x%x   ---  READ_BUF[%d]: 0x%x 0x%x 0x%x 0x%x !\n\n", b,
									*((int *)&(expected_data_block[b])), *((int *)&(expected_data_block[b+4])),
									*((int *)&(expected_data_block[b+8])), *((int *)&(expected_data_block[b+12])), b,
									*((int *)&(real_data_block[b])), *((int *)&(real_data_block[b+4])),
									*((int *)&(real_data_block[b+8])), *((int *)&(real_data_block[b+12])) );
						} else {
							fprintf( stderr, ">>> EXPECT_BUF[%d]: %c ---  READ_BUF[%d]: %c !\n\n",
									b, expected_data_block[b], b, real_data_block[b] );
						}

						retries = 0;
						errcount++;
						if (errcount > 3) {
							fprintf( stderr, "%s: Too many failures (3)... exiting...\n", argv[0]);
							printf("//dcc:dev='%s',pat=0x%llx,cycles=%d/%d,wpass=%d/%d,rpass=%d/%d,mbrange=%lld,bsize=%d,"
								   "blkno=%lld/%lld,wusec=%lld,rusec=%lld,errcount=1,error='%s'\n",
									devname, pattern_seed, cycle, cycles, wrp, wr_passes, rrp, rd_passes, mbrange,
									BLOCKSIZE, bn, max_baddr, wr_usecs, rd_usecs, "Too many (3) read verification failures" );
							return -1;
						}
					}

				}
				printf("Read pass %d of %d Completed OK\n", rrp + 1, rd_passes );

				if (errcount > 0 ) {
					printf("//dcc:dev='%s',pat=0x%llx,cycles=%d/%d,wpass=%d/%d,rpass=%d/%d,mbrange=%lld,bsize=%d,"
						   "blkno=%lld/%lld,wusec=%lld,rusec=%lld,errcount=%d,error='%s'\n",
							devname, pattern_seed, cycle, cycles, wrp, wr_passes, rrp, rd_passes, mbrange,
							BLOCKSIZE, bn, max_baddr, wr_usecs, rd_usecs, errcount, "Read verification failed" );
					fprintf( stderr, "%s: Read verification failed... exiting...\n", argv[0]);
					return -1;
				}
			}

			stop_timer();
			print_results( "READ Passes", rd_passes, &rd_usecs );

			printf("All %d read passes completed OK!\n", rd_passes );
		}
	}

	printf("All %d write/read verification cycles completed OK!\n", cycles );
	printf("//dcc:dev='%s',pat=0x%llx,cycles=%d/%d,wpass=%d/%d,rpass=%d/%d,mbrange=%lld,bsize=%d,"
		   "blkno=%lld/%lld,wusec=%lld,rusec=%lld,errcount=0,error='%s'\n",
			devname, pattern_seed, cycle, cycles, wrp, wr_passes, rrp, rd_passes, mbrange, BLOCKSIZE, bn,
			max_baddr, wr_usecs, rd_usecs, "OK" );

	return 0;
}

